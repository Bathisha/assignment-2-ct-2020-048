# Weather Viewer in C - Installation and Usage Guide

## Overview

The Weather Viewer is a C language program that allows users to view the current temperature, wind speed, and wind direction. This guide provides instructions for installing the required dependencies and building the program on a Linux x86_64 system.

## Installation

Before building the Weather Viewer program, you need to install two external libraries: cURL and cJSON. Follow the steps below to install these dependencies.

### 1. Install cURL

To install the cURL library, use your package manager. Here's how to install it on Arch Linux:

```bash
# pacman -S lib32-curl
```

### 2. Install cJSON

To install the cJSON library, use your package manager. Here's how to install it on Arch Linux:

```bash
# pacman -S cjson
```

## Building the Weather Viewer

Once you have installed the required dependencies, you can proceed to build the Weather Viewer program. Any C language compiler can be used to build the program. The following example demonstrates how to build it using the GNU C compiler:

```bash
$ gcc weather.c -lcurl -lcjson -o weather
```

## Usage

The Weather Viewer allows you to check weather information for either your location or someone else's location, based on their IP address or latitude and longitude. Below are the usage options:

```bash
Usage: ./weather
       ./weather <ip address>
       ./weather <latitude> <longitude>
```

To use the program, execute the `weather` binary followed by the desired option based on your requirements. If you don't provide any arguments, the program will display the weather information for your current location.

Please note that this program has been tested on Linux x86_64 systems. Ensure that the external libraries are installed correctly and that you have appropriate permissions to access weather data based on IP addresses or coordinates.

By following these instructions, you can successfully install the Weather Viewer and utilize it to view current temperature, wind speed, and wind direction in a user-friendly manner.