#include <cjson/cJSON.h>
#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned long write_response(void *c, unsigned long s, unsigned long m, char **o) {
    unsigned long size = s * m;

    *o = (char*)malloc(size + 1);
    if (!*o) {
        printf("Something went wrong while allocating memory for response.\n");
        exit(1);
    }

    memcpy(*o, c, size);

    (*o)[size] = '\0';

    return size;
}

char* send_request(char *u, unsigned long s) {
    CURL *curl_request = curl_easy_init();
    if (!curl_request) {
        printf("Something went wrong while initiating cURL.\n");
        exit(1);
    }

    char* response = NULL;

    curl_easy_setopt(curl_request, CURLOPT_URL, u);
    curl_easy_setopt(curl_request, CURLOPT_WRITEFUNCTION, write_response);
    curl_easy_setopt(curl_request, CURLOPT_WRITEDATA, &response);

    CURLcode curl_response = curl_easy_perform(curl_request);
    if (curl_response != CURLE_OK) {
        printf("Something went wrong while sending cURL request.\n");
        curl_easy_cleanup(curl_request);
        exit(1);
    }

    curl_easy_cleanup(curl_request);

    return response;
}

int main(int c, char* v[]) {
    char weather_url[256];

    char* weather = NULL;

    if (c > 3) {
        printf("Too many arguments.\n");
        printf("Usage: weather | weather <ip address> | weather <latitude> <longitude>\n");
        exit(1);
    }

    if (c == 3) {
        sprintf(weather_url, "https://api.open-meteo.com/v1/forecast?latitude=%s&longitude=%s&current_weather=true", v[1], v[2]);

        weather = send_request(weather_url, 256);
    }

    if (c == 2) {
        char latitude_url[256];
        char longitude_url[256];

        sprintf(latitude_url, "https://ipapi.co/%s/latitude", v[1]);
        sprintf(longitude_url, "https://ipapi.co/%s/longitude", v[1]);

        char* latitude = send_request(latitude_url, 256);
        char* longitude = send_request(longitude_url, 256);

        sprintf(weather_url, "https://api.open-meteo.com/v1/forecast?latitude=%s&longitude=%s&current_weather=true", latitude, longitude);

        weather = send_request(weather_url, 256);

        free(latitude);
        free(longitude);
    }

    if (c == 1) {
        char* latitude = send_request("https://ipapi.co/latitude", 256);
        char* longitude = send_request("https://ipapi.co/longitude", 256);

        sprintf(weather_url, "https://api.open-meteo.com/v1/forecast?latitude=%s&longitude=%s&current_weather=true", latitude, longitude);

        weather = send_request(weather_url, 256);

        free(latitude);
        free(longitude);
    }

    if (weather == NULL) {
        printf("Something went wrong while getting weather.\n");
        exit(1);
    }

    cJSON* json = cJSON_Parse(weather);
    if (json == NULL) {
        printf("Something went wrong while parsing JSON data.\n");
        exit(1);
    }

    cJSON* current_weather = cJSON_GetObjectItemCaseSensitive(json, "current_weather");
    cJSON* temperature = cJSON_GetObjectItemCaseSensitive(current_weather, "temperature");
    cJSON* wind_speed = cJSON_GetObjectItemCaseSensitive(current_weather, "windspeed");
    cJSON* wind_direction = cJSON_GetObjectItemCaseSensitive(current_weather, "winddirection");

    printf("Temperature: %.1f C\n", temperature->valuedouble);
    printf("Wind speed: %.1f Km/h\n", wind_speed->valuedouble);
    printf("Wind direction: %.1f degrees\n", wind_direction->valuedouble);
    cJSON_Delete(json);
    free(weather);

    return 0;
}
